public abstract class Account {

    private MonetaryAmount balance;
    private String clientNumber;

    public Account(String clientNumber, String currency) {
        this.clientNumber = clientNumber;
        this.balance = new MonetaryAmount(0, currency);
    }

    public MonetaryAmount getCurrentBalance() { return this.balance; }

    public String getClientNumber() { return this.clientNumber; }

    public void deposit(double amount) {
        this.getCurrentBalance().addAmount(amount);
    }

    public abstract void withdraw(double amount) throws Exception;
}
