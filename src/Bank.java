import java.util.ArrayList;
import java.util.List;

public class Bank {
    public List<Client> clients;

    public List<Client> getClients() {
        return clients;
    }

    public Bank() {
        clients = new ArrayList<>();
    }



    public void addAClient(Client client) {
        this.clients.add(client);
    }
}
