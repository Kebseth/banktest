public class Client {

    private String name;
    private String id;
    private Bank bank;

    public Client(String name, Bank bank) {
        this.name = name;
        this.bank = bank;
        this.id = "0";

        // Vérification du numero client s'il existe ou qu'il est unique
        while (this.id == "0" || !verifyClientNumber(this.id) ) {
            // Création d'un numero client
            int random = getRandomNumber(1, 99999999);
            this.id = String.format("%08d" , random);
        }
            this.bank.addAClient(this);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {return this.id;}

    public final static int getRandomNumber(int min, int max){
        int x = (int) ((Math.random()*((max-min)+1))+min);
        return x;
    }

    public Boolean verifyClientNumber(String id) {
        if(bank.getClients() != null) {
            for (Client cli:bank.getClients()) {
                if(id == cli.id) {
                    return false;
                }
            }
        } return true;
    }
}
