import org.junit.jupiter.api.BeforeAll;

public class ClientTest {

    public Bank bank;
    public Client obelix;
    public SavingAccount obelixSaving;
    public CurrentAccount obelixCurrent;

    @BeforeAll
    void setUp() {
        bank = new Bank();
        obelix = new Client("Obelix", bank);
        obelixSaving = new SavingAccount(obelix.getId(), "Euros", 0.01);
        obelixCurrent = new CurrentAccount(obelix.getId(), "Euros");
    }
}
