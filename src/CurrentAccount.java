public class CurrentAccount extends Account {

    private MonetaryAmount balance;

    public CurrentAccount(String clientNumber, String currency) {
        super(clientNumber, currency);
        this.balance = new MonetaryAmount(0, currency);
    }

    public void withdraw(double amount) {
        if((this.getCurrentBalance().getAmount() - amount) >= -1000) {
            this.getCurrentBalance().substractAmount(amount);
        } else {
            System.out.println("Vous ne pouvez pas être à plus de 1000 " + this.getCurrentBalance().getCurrency() + " de découvert.");
        }
    }
}
