import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CurrentAccountTest {

    public Account tom;

    @BeforeEach
    void setUp() {
        tom = new CurrentAccount("09870845", "Euros");
    }

    @Test
    void isCurrentBalanceWorks()  {
        Assertions.assertTrue(tom.getCurrentBalance() instanceof MonetaryAmount);
    }

    @Test
    void isDepositWorks() {
        tom.deposit(204);
        Assertions.assertEquals(204.0, tom.getCurrentBalance().getAmount());
    }

    @Test
    void isDepositFails() {
        tom.deposit(204);
        Assertions.assertNotEquals(205.0, tom.getCurrentBalance().getAmount());
    }

    @Test
    void isWithdrawWorks() throws Exception {
        tom.withdraw(100);
        Assertions.assertEquals(-100.0, tom.getCurrentBalance().getAmount());
    }

    @Test
    void isWithdrawFailWithNoBalance() throws Exception {
        tom.withdraw(1200);
        Assertions.assertEquals(0.0, tom.getCurrentBalance().getAmount());
    }

    @Test
    void isTheClientNumberIsReturn() {
        Assertions.assertEquals("09870845", tom.getClientNumber());
    }
}
