public class Main {
    public static void main(String[] args) {
        Bank bank = new Bank();
        Client obelix = new Client("Obelix", bank);
        System.out.println(obelix.getId());

        SavingAccount obelixSaving = new SavingAccount(obelix.getId(), "Euros", 0.01);
        // obelixSaving.withdraw(100);
        System.out.println(obelixSaving.getCurrentBalance().getAmount());

        CurrentAccount obelixCurrent = new CurrentAccount(obelix.getId(), "Euros");
        obelixCurrent.withdraw(1200);
        System.out.println(obelixCurrent.getCurrentBalance().getAmount());
    }
}
