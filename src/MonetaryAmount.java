public class MonetaryAmount {
    private double amount;
    private String currency;

    public MonetaryAmount(double amount, String currency){
        this.amount = amount;
        this.currency = currency;
    }

    public double getAmount() { return this.amount; }

    public void addAmount(double a) {
        amount += a;
    }

    public void substractAmount(double a) {
        amount -= a;
    }

    public void setAmount(double amount) { this.amount = amount; }

    public String getCurrency() {
        return this.currency;
    }

    public String toStringy() {
        return "Your amount has " + this.getAmount() + " " + this.currency + ".";
    }

}
