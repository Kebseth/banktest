import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MonetaryAmountTest {

    public MonetaryAmount amount;

    @BeforeEach
    void setUp() {
        amount = new MonetaryAmount(200, "Euros");
    }

    @Test
    void isGetAmountWorks() {
        Assertions.assertEquals(200.0, amount.getAmount());
    }


    @Test
    void iSAddAmountWorks() {
        amount.addAmount(300);
        Assertions.assertEquals(500.0, amount.getAmount() );
    }

    @Test
    void iSAddAmountFails() {
        amount.addAmount(300);
        Assertions.assertNotEquals(600.0, amount.getAmount() );
    }

    @Test
    void isSubstractAmountWorks() {
        amount.substractAmount(300);
        Assertions.assertEquals(-100.0, amount.getAmount());
    }

    @Test
    void isSubstractAmountFails() {
        amount.substractAmount(200);
        Assertions.assertNotEquals(-100.0, amount.getAmount());
    }

    @Test
    void isToStringDisplayTheGoodSentence() {
        Assertions.assertEquals("Your amount has 200.0 Euros.", amount.toStringy());
    }
}
