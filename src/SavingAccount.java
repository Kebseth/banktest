public class SavingAccount extends Account {

    private double interest;

    public SavingAccount(String clientNumber, String currency, double interest) {
        super(clientNumber, currency);
        this.getCurrentBalance().setAmount(50);
        this.interest = interest;
    }

    public double getInterest() { return this.interest ;}

    public void withdraw(double amount) throws Exception{
        if((this.getCurrentBalance().getAmount() - amount) >= 0) {
            this.getCurrentBalance().substractAmount(amount);
        } else {
            System.out.println("Vous ne pouvez pas avoir de découvert.");
        }
    }

    public void interestCredit() {
        double actual = this.getCurrentBalance().getAmount();
        double interest = actual * this.getInterest();
        this.getCurrentBalance().setAmount(actual + interest);
    }
}
