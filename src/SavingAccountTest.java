import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SavingAccountTest {

    public SavingAccount tom;

    @BeforeEach
    void setUp() {
        tom = new SavingAccount("09870845", "Euros", 0.01);
    }

    @Test
    void isGetInterestWorks() {
        Assertions.assertEquals(0.01, tom.getInterest());
    }

    @Test
    void isDepositWorks() {
        tom.deposit(204);
        Assertions.assertEquals(254.0, tom.getCurrentBalance().getAmount());
    }

    @Test
    void isWithdrawWithNoBalanceFails() throws Exception {
        tom.withdraw(100);
        Assertions.assertEquals(50.0, tom.getCurrentBalance().getAmount());
    }

    @Test
    void isWithdrawWorks() throws Exception {
        tom.withdraw(40);
        Assertions.assertEquals(10.0, tom.getCurrentBalance().getAmount());
    }

    @Test
    void isInterestCreditWorks() {
        tom.deposit(10000);
        tom.interestCredit();
        Assertions.assertEquals(10150.5, tom.getCurrentBalance().getAmount());
    }
}
